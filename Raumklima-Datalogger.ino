// Raumklima-Datalogger
//
// (C) Dr. Matthias Offer
// Juli 2013
// Version 1.4Mega

// Infos:
// -----------------------------------------------------------------------
// Connect pin 1 (on the left) of the sensor to +5V
// Connect pin 2 of the sensor to whatever your DHTPIN is
// Connect pin 4 (on the right) of the sensor to GROUND
// Connect a 10K resistor from pin 2 (data) to pin 1 (power) of the sensor


// Laden der System-Bibliotheken
// -----------------------------------------------------------------------
#include "DHT.h"
#include "Wire.h" 
#include "LiquidCrystal_I2C.h"
#include "RTClib.h"
#include "SD.h"

// Globale Systemvariablen
// -----------------------------------------------------------------------
#define DHTTYPE DHT22               // Typ der Sensoren -> DHT 22  (AM2302)
#define DHTPIN 9                    // DigitalPIN des Innensensors
#define Displaydelay 5              // Anzeigedauer in s von Displayinformationen
#define chipSelect 10

int Save_delay = 1200;              // Speicherintervall in Sekunden

uint16_t Wartesekunde;

String inputString = "";            // Variable für ankommende Befehle

boolean inputstringComplete = false;// Ist der Eingabestring komplett?
boolean StatusSDKarte;              // Status der SD-Karte
boolean toSave = true;              // True = es wird auf die SD-Karte gespeichert

File dataFile;                      // Objekt für den Dateinamen

DHT dht_Sensor(DHTPIN, DHTTYPE);    // Objekt für den Sensor

RTC_DS1307 RTC;                     // Typ der RTC

LiquidCrystal_I2C lcd(0x27,16,2);   // LCD-Addresse: 0x27 16x2 Zeichen


// Stellt den Namen und die Version des Reglers auf dem Display da
// -----------------------------------------------------------------------
void VersionInfo(){
  lcd.print("Datalogger V1.4M");
  lcd.setCursor(0, 1);
  lcd.print("(C) Dr. M.Offer");
}


// Stellt das aktuelle Datum als String da
// -----------------------------------------------------------------------
String Date_String(uint8_t day, uint8_t month, uint16_t year) {
  String DATE;
  String DAY;
  String MONTH;
  String YEAR;
  
  if (day < 10){DAY="0"+String(day, DEC);}
  else{DAY=String(day, DEC);}
  
  if (month < 10){MONTH = "0"+String(month, DEC);}
  else{MONTH = String(month, DEC);}

  YEAR=String(year, DEC);
  
  DATE = DAY+"."+MONTH+"."+YEAR;
  
  return DATE;
}


// Stellt die aktuelle Uhrzeit als String da
// -----------------------------------------------------------------------
String Time_String(uint8_t hour, uint8_t minute) {
  String TIME;
  String HOUR;
  String MINUTE;
  
  if (hour < 10){HOUR="0"+String(hour, DEC);}
  else{HOUR=String(hour, DEC);}
  
  if (minute < 10){MINUTE = "0"+String(minute, DEC);}
  else{MINUTE = String(minute, DEC);}
  
  TIME = HOUR+":"+MINUTE;
  
  return TIME;
}


// Stellt das aktuelle Datum und die Uhrzeit auf dem Display da
// -----------------------------------------------------------------------
void LCD_Display_Time(String Datum, String Uhrzeit) {
  lcd.home();
  lcd.print(F("Datum:"));
  lcd.print(Datum);
  lcd.setCursor(0, 1);
  lcd.print(F("Zeit :"));
  lcd.print(Uhrzeit);
}


// Stellt die aktuellen Messdaten auf dem Display da
// -----------------------------------------------------------------------
void LCD_Display_Temp_and_Hum(float Temp, float Hum){
  lcd.home();
  lcd.print(F("Temp   : "));
  lcd.print(Temp);
  lcd.write(223);
  lcd.print(F("C"));
  lcd.setCursor(0, 1);
  lcd.print(F("Feuchte: "));
  lcd.print(Hum);
  lcd.print(F("%  "));
}


// Setup des bluetooth-Shields
// -----------------------------------------------------------------------
void setupBlueToothConnection()
{
  Serial1.begin(38400); //Set BluetoothBee BaudRate to default baud rate 38400
  Serial1.print("\r\n+STWMOD=0\r\n"); //set the bluetooth work in slave mode
  Serial1.print("\r\n+STNA=Datalogger_R318\r\n"); //set the bluetooth name as "Datalogger_R318"
  Serial1.print("\r\n+STOAUT=1\r\n"); // Permit Paired device to connect me
  Serial1.print("\r\n+STAUTO=0\r\n"); // Auto-connection should be forbidden here
  delay(2000); // This delay is required.
  Serial1.print("\r\n+INQ=1\r\n"); //make the slave bluetooth inquirable 
  //Serial.println("The slave bluetooth is inquirable!");
  delay(2000); // This delay is required.
  Serial1.flush();
}


// Liest einen String über das bluetooth-Shield
// -----------------------------------------------------------------------
void serialEvent() {
  while (Serial1.available()) {
    char inChar = (char)Serial1.read();
    if (!(inChar == '\n')) {
      if (!(inChar =='\r')){
        inputString += inChar;    
      }
    }
    else {
      inputstringComplete = true;
    }
  }
}


// Setzt die String Variable zurück
// -----------------------------------------------------------------------
void ClearInputStr() {
  inputString = "";
  inputstringComplete = false;
}


// Setup des Programms
// -----------------------------------------------------------------------
void setup() {
  Wire.begin();
  RTC.begin();
  dht_Sensor.begin();
  setupBlueToothConnection();
  
  inputString.reserve(15);         // Reserviert 15 bytes für inputString
    
  pinMode(SS, OUTPUT);
  
  // Stellt die Echtzeituhr beim Upload des Programms auf die Rechnerzeit
  if (! RTC.isrunning()) {
    RTC.adjust(DateTime(__DATE__, __TIME__));
  }
  
  lcd.init();                      // Initialisiert das LCD-Display
  lcd.backlight();                 // Intergrundbeleuchtung ist AN
  
  VersionInfo();                   // Stellt den Namen und die Version des Reglers auf dem Display da
  delay(Displaydelay*1000);
  lcd.clear();
  
  if (!SD.begin(10,11,12,13)) {
    lcd.clear();
    lcd.print(F("Keine SD-Karte"));
    lcd.setCursor(0, 1);
    lcd.print(F("Logger AUS!"));
    while (1);
  }
  
  if (! SD.exists("datalog.txt")) {
    dataFile = SD.open("datalog.txt", FILE_WRITE);
    dataFile.println(F("Raumklima-Datalogger"));
    dataFile.println(F("(C) Dr. Matthias Offer"));
    dataFile.println();
    dataFile.println(F("Spalte 1: Datum"));
    dataFile.println(F("Spalte 2: Uhrzeit"));
    dataFile.println(F("Spalte 3: Zeitstempel(s) (Unix-Time UTC/GMT)"));
    dataFile.println(F("Spalte 4: Luftfeuchte(%)"));
    dataFile.println(F("Spalte 5: Temperatur(°C)"));
    dataFile.println();
    dataFile.println(F("-------------------------------------------------------------------------------------"));
    dataFile.println();
    dataFile.close();
  } 
}


void loop() {
  DateTime now = RTC.now();
   
  unsigned long uTime = now.unixtime();
  
  int Sekunde = now.second();
  
  float Hum = dht_Sensor.readHumidity();
  float Temp = dht_Sensor.readTemperature();
  
  if (isnan(Temp) || isnan(Hum)) {
    lcd.clear();
    lcd.print(F("Fehler beim"));
    lcd.setCursor(0, 1);
    lcd.print(F("Sensor!"));
    
    while(1);
    } 
    
    else {
      if (Sekunde % (Displaydelay*2) == 0) {
        LCD_Display_Temp_and_Hum(Temp, Hum);
        Wartesekunde = Sekunde;
      }   
  }
  
  if (Sekunde >= Wartesekunde + Displaydelay) {
    LCD_Display_Time(Date_String(now.day(), now.month(), now.year()), Time_String(now.hour(), now.minute())+"    ");
  }
  
  if (uTime % Save_delay == 0) {
    if (toSave == true) {
      File dataFile = SD.open("datalog.txt", FILE_WRITE);
      if (dataFile) {
        dataFile.print(Date_String(now.day(), now.month(), now.year()));
        dataFile.print("\t");
        dataFile.print(Time_String(now.hour(), now.minute()));
        dataFile.print("\t");
        dataFile.print(uTime);
        dataFile.print("\t");
        dataFile.print(Hum);
        dataFile.print("\t");
        dataFile.println(Temp);
        dataFile.close();
      }
      toSave = false;
    }
  }
  else {
    toSave = true;
  }
  
  serialEvent();
  
  if (inputstringComplete) {

    //command: init
    if (inputString=="init"){
      Serial1.println(F("Datenlogger V1.4MEGA (C) Dr. Matthias Offer ok"));
    }
    //command: get:hum
    else if (inputString=="get:hum"){
      Serial1.print(Hum);
      Serial1.println(" ok");
    }
    //command: get:temp
    else if (inputString=="get:temp"){
      Serial1.print(Temp);
      Serial1.println(" ok");
    }
    //command: get:alldata
    else if (inputString=="get:alldata"){
      Serial1.print(uTime);
      Serial1.print(" ");
      Serial1.print(Date_String(now.day(), now.month(), now.year()));
      Serial1.print(" ");
      Serial1.print(Time_String(now.hour(), now.minute()));
      Serial1.print(" ");
      Serial1.print(Hum);
      Serial1.print(" ");
      Serial1.print(Temp);
      Serial1.print(" ");
      Serial1.print(Save_delay);
      Serial1.println(" ok");
    }
    //command: get:date
    else if (inputString=="get:date"){
      Serial1.print(Date_String(now.day(), now.month(), now.year()));
      Serial1.println(" ok");
    }
    //command: get:time
    else if (inputString=="get:time"){
      Serial1.print(Time_String(now.hour(), now.minute()));
      Serial1.println(" ok");
    }
    //command: get:timestamp
    else if (inputString=="get:timestamp"){
      Serial1.print(uTime);
      Serial1.println(" ok");
    }
    //command: get:savedelay
    else if (inputString=="get:savedelay"){
      Serial1.print(Save_delay);
      Serial1.println(" ok");
    }
    //command: set:
    else if (inputString.startsWith("set:")){
      inputString=inputString.substring(4);
        //command: set:savedelayX X in Sekunden
        if (inputString.startsWith("savedelay")){
          inputString=inputString.substring(9);
          Save_delay=inputString.toInt();
          Serial1.print(Save_delay);
          Serial1.println(" ok");
        }
        //command: set:utimeXXXXXXXXXX X = Unix Zeitstempel als UTC/GMT
        if (inputString.startsWith("utime")){
          inputString=inputString.substring(5);
          if(inputString.length()>=8){
            RTC.adjust(DateTime(inputString.toInt()));
            Serial1.print(inputString.toInt());
            Serial1.println(" ok");
          }
          else {
            Serial1.println("command unknown");
          }
        }
      }
    else {
      Serial1.println("command unknown");
    }
    
  ClearInputStr();
  }
}
